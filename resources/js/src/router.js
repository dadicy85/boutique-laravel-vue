import Vue from 'vue';
import VueRouter from 'vue-router';
import Error from './components/Error.vue';
import BoutiquePrincipal from './components/BoutiquePrincipal.vue';
import Panier from './components/Panier/Panier.vue';


Vue.use(VueRouter);

export default new VueRouter({
	mode:'history',
	routes: [
		{path:'', redirect:'/boutique'},
		{path:'/boutique', component:BoutiquePrincipal, name:'boutique'},
		{path:'/panier', component:Panier, name:'panier'},
		{path:'*', component:Error}
	]
})