import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
Vue.config.productionTip = false

export const eventBus = new Vue({
  data: {
    produits: [
      {
        id: '1',
        img: 'https://medias.maisonsdumonde.com/image/upload/q_auto,f_auto/w_2000/img/fauteuil-en-rotin-et-metal-noir-1000-0-23-155618_1.jpg',
        titre: 'Fauteuil en osier',
        description: 'Avec son assise en rotin, le fauteuil en rotin PITAYA apportera une touche d exotisme dans votre interieur.',
        prix: 199
      },
      {
        id: '2',
        img: 'https://medias.maisonsdumonde.com/image/upload/q_auto,f_auto/w_2000/img/armoire-2-portes-2-tiroirs-noire-1000-15-17-116006_1.jpg',
        titre: 'Armoire',
        description: 'Avec son assise en rotin, le fauteuil en rotin PITAYA apportera une touche d exotisme dans votre interieur.',
        prix: 800
      },
      {
        id: '3',
        img: 'https://medias.maisonsdumonde.com/image/upload/q_auto,f_auto/w_2000/img/buffet-4-tiroirs-en-manguier-massif-sculpte-1000-13-28-175385_1.jpg',
        titre: 'Buffet',
        description: 'Avec son assise en rotin, le fauteuil en rotin PITAYA apportera une touche d exotisme dans votre interieur.',
        prix: 600
      },
      {
        id: '4',
        img: 'https://medias.maisonsdumonde.com/image/upload/q_auto,f_auto/w_2000/img/table-a-manger-10-personnes-en-chene-massif-et-metal-noir-l225-1000-7-29-187194_1.jpg',
        titre: 'Table',
        description: 'Avec son assise en rotin, le fauteuil en rotin PITAYA apportera une touche d exotisme dans votre interieur.',
        prix: 1000
      },
      {
        id: '5',
        img: 'https://medias.maisonsdumonde.com/image/upload/q_auto,f_auto/w_2000/img/chaise-noire-et-pieds-en-metal-noir-1000-12-22-198011_1.jpghttps://medias.maisonsdumonde.com/image/upload/q_auto,f_auto/w_2000/img/fauteuil-en-rotin-et-metal-noir-1000-0-23-155618_1.jpg',
        titre: 'Chaise',
        description: 'Avec son assise en rotin, le fauteuil en rotin PITAYA apportera une touche d exotisme dans votre interieur.',
        prix: 59
      },
      {
        id: '6',
        img: 'https://medias.maisonsdumonde.com/image/upload/q_auto,f_auto/w_2000/img/etagere-en-metal-noir-1000-13-21-198420_1.jpg',
        titre: 'Etagere',
        description: 'Avec son assise en rotin, le fauteuil en rotin PITAYA apportera une touche d exotisme dans votre interieur.',
        prix: 270
      }      
    ],
    panier: []
  },
  methods: {
    ajouterProduitAuPanier(produit) {
      if(!this.panier.map(i=>i.id).includes(produit.id)){
        this.panier = [...this.panier, produit];
        this.$emit('update:panier', this.panier.slice());
      }
    },
    retirerProduitDuPanier(item) {
      this.panier = this.panier.slice().filter(i => i.id !== item.id);
      this.$emit('update:panier', this.panier.slice());
    }
  }
})

new Vue({
  router:router,
  render: h => h(App),
}).$mount('#app')
